Fighter Classes
~~~~~~~~~~~~~~~

Classes based on this chassis primarily focus on strength or constitution for their primary effects. They will almost all have high hit-points and high survivability, but they may lack mobility or ways to interact with a target at range. They have:

    * Full BaB
    * Strong Fort Save, weak Will and Reflex saves
    * d10 hit die

.. toctree::
    :maxdepth: 1
    :glob:

    fighter/*
