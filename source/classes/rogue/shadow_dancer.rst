Shadow Dancer [INCOMPLETE]
~~~~~~~~~~~~~~~~~~~~~~~~~~

Chassis: :doc:`Rogue </classes/rogue>`
Built from: `DotA2 -- Slark <https://dota2.gamepedia.com/Slark>`_
Play style: Highly mobile melee ganker; TBA

Move Speed: 30

Skills: Appraise, Balance, Bluff, Climb, Escape Artist, Hide, Jump, Move Silently, Sense Motive, Swim, Sleight of Hand, Tumble, Use Magic Device

Skill Points: 8

.. _Save DC:

Save DC: 10 +  1/2 your Shadow Dancer level + your cha mod

+-------+--------------------------------+
| Level | Gain                           |
+=======+================================+
| 1     | :term:`Tendrils of Hatset`     |
+-------+--------------------------------+
| 2     | :term:`Strike from Shadow`     |
+-------+--------------------------------+
| 3     | :term:`Gaze of Hatset`         |
+-------+--------------------------------+
| 4     | :term:`Ancient Bargain`        |
+-------+--------------------------------+
| 5     | :term:`Talent for Mischief`    |
+-------+--------------------------------+
| 6     | :term:`Dance of the Shadows`   |
+-------+--------------------------------+
| 7     | :term:`` |
+-------+--------------------------------+
| 8     | |l8|  |
+-------+--------------------------------+
| 9     | |l9|  |
+-------+--------------------------------+
| 10    | |l10| |
+-------+--------------------------------+
| 11    | |l11| |
+-------+--------------------------------+
| 12    | |l12| |
+-------+--------------------------------+
| 13    | |l13| |
+-------+--------------------------------+
| 14    | |l14| |
+-------+--------------------------------+
| 15    | |l15| |
+-------+--------------------------------+
| 16    | |l16| |
+-------+--------------------------------+
| 17    | |l17| |
+-------+--------------------------------+
| 18    | |l18| |
+-------+--------------------------------+
| 19    | |l19| |
+-------+--------------------------------+
| 20    | |l20| |
+-------+--------------------------------+

.. glossary::

    Tendrils of Hatset
        Every successful melee attack you make against a creature inflicts the :term:`Tendrils` status effect for 1d4 rounds. This duration does not stack. Instead, if the creature already has the :term:`Tendrils` effect, that effect ends immediately and a new :term:`Tendrils` effect is applied with the new duration.

    Tendrils
        Status Effect: Can be dispelled by `Dispel Magic <dispel magic_>`_ or similar effects; the DC for this is equal to your `Save DC`_. If a creature with more hit dice than you would die while under this effect, it must first make a Reflex save against this effect, you permanently gain 1 point of bonus Dexterity.

    Strike from Within
        Gain the :term:`Strike` supernatural ability.

    Strike
        Supernatural Ability: As a standard action, travel through shadows to arrive in a square adjacent to target creature. You may travel in a straight line to a distance equal to your move speed. You must have line of effect to your target in order to use this ability. Your target must make a Reflex save against your `Save DC`_ or be `Entangled <entangle_>`_ by tentacles for a number of rounds equal to 2 + 1/4 your Shadow Dancer level, rounded down. If there is a suitable surface or object within 5ft of the target creature, the tentacles are anchored to that surface; otherwise they are not anchored. This ability recharges on a short rest.

    Gaze of Hatset
        Whenever you successfully critical strike a creature with the :term:`Tendrils` status effect, the new :term:`Tendrils` effect that you inflict also causes ability drain equal to 1 + 1/4 your Shadow Dancer level, rounded down, to a randomly selected attribute.

    Ancient Bargain
        Gain the :term:`Bargain` spell.

    Bargain
        Spell: As a swift action, gain the :term:`Negotiating` status effect until the end of your next turn. Multiple applications of this effect do not stack. Instead, the previous one ends prematurely and is replaced with the new one.

    Negotiating
        Status Effect: Can be dispelled by `Dispel Magic <dispel magic_>`_ or similar effects; the DC for this is equal to your `Save DC`_. This effect does nothing while active. When its duration is complete, it deals negative energy damage equal to Nd6, where N is 1/3 your Shadow Dancer level, rounded down, to all creatures within 5ft of you that fail a Reflex save against your `Save DC`_, and half that much damage to you. This means that if ``Negotiating`` is dispelled or otherwise ends prematurely (such as if you die), no damage is dealt. The damage to yourself cannot be prevented or reduced by any means.

    Talent for Mischief
        Permanently add 1 point to your Charisma or your Constitution.

    Dance of the Shadows
        Gain the :term:`Shadow Dance` spell and the :term:`Always Be Hiding` passive ability.
    
    Shadow Dance
        Spell: As a swift action, gain the :term:`Dancing` status effect for a number of rounds equal to 2 + 1/3 your Shadow Dancer level, rounded down. This ability recharges after a long rest.

    Dancing
        Status Effect: Can be dispelled by `Dispel Magic <dispel magic_>`_ or similar effects; the DC for this is equal to your `Save DC`_. While under this effect, a cloud of shadows surrounds you and you gain `total cover <cover_>`_ from all creatures.

    Always Be Hiding
        Passive Supernatural Ability: At any time when you are hidden from all enemies, gain the following bonuses:

        * 20% increased move speed, rounded down to the nearest 5ft.
        * Regenerate hit points at a rate equal to 1 hit point per round per your Shadow Dancer level.

        This effect can be dispelled by `Dispel Magic <dispel magic_>`_ or similar effects; the DC for this is equal to your `Save DC`_. If dispelled, it returns after a long rest. Note that if an enemy is not present but is watching you via magical means, as with `Scrying <scrying_>`_ or a similar effect, you do are not hidden and are thus denied the bonuses listed above.

    

    

.. _dispel magic: https://www.d20srd.org/srd/spells/dispelMagic.htm
.. _entangle: https://www.d20srd.org/srd/conditionSummary.htm#entangled
.. _cover: https://www.d20srd.org/srd/combat/combatModifiers.htm#cover
.. _scrying: https://www.d20srd.org/srd/spells/scrying.htm
        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        

        
        
