Rogue Classes
~~~~~~~~~~~~~

Classes based on this chassis primarily focus on dexterity or charisma for their primary effects. They will almost all have high mobility and high damage output, but they may lack strong defenses. They have:

    * Full BaB
    * Strong Ref Save, weak Fort and Will saves
    * d8 hit die

.. toctree::
    :maxdepth: 1
    :glob:

    rogue/*