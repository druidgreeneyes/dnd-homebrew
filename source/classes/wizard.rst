Wizard Classes
~~~~~~~~~~~~~~

Classes based on this chassis primarily focus on intelligence or wisdom for their primary effects. They will generally have string abilities for their particular focus, but they may not scale as strongly or be as survivable as Fighter or Rogue classes:

    * Full BaB
    * Strong Will save, weak Fort and Ref saves.
    * d6 hit die

.. toctree::
    :maxdepth: 1
    :glob:

    wizard/*