Nightjack
~~~~~~~~~

Chassis: :doc:`Fighter </classes/fighter>`
Built from: `DotA2 -- Lifestealer <https://dota2.gamepedia.com/Lifestealer>`_
Play style: Hyper-aggressive melee combatant; stealthy, back-line social influencer.

Move Speed: 30

Skills: Climb, Intimidate, Jump, Listen, Sleight of Hand, Spot, Survival, Swim, Use Magic Device

Skill Points: 4

.. _Save DC:

Save DC: 10 +  1/2 your Nightjack level + your con mod

+-------+--------------------------------+
| Level | Gain                           |
+=======+================================+
| 1     | :term:`Consumption`            |
+-------+--------------------------------+
| 2     | :term:`Curse of Twilight`      |
+-------+--------------------------------+
| 3     | :term:`Consumption of Soul`    |
+-------+--------------------------------+
| 4     | :term:`Cloak of Shadows`       |
+-------+--------------------------------+
| 5     | :term:`Talent for Eating`      |
+-------+--------------------------------+
| 6     | :term:`Soul Portal`            |
+-------+--------------------------------+
| 7     | :term:`Consumption of Essence` |
+-------+--------------------------------+
| 8     | :term:`Cloak of Starlight`     |
+-------+--------------------------------+
| 9     | :term:`Curse from Afar`        |
+-------+--------------------------------+
| 10    | :term:`Talent for Mayhem`      |
+-------+--------------------------------+
| 11    | :term:`Eldritch Forces`        |
+-------+--------------------------------+
| 12    | :term:`Theater`                |
+-------+--------------------------------+
| 13    | :term:`Curse of Vulnerability` |
+-------+--------------------------------+
| 14    | :term:`Cloak of Infinity`      |
+-------+--------------------------------+
| 15    | :term:`Talent for Survival`    |
+-------+--------------------------------+
| 16    | :term:`Consumption of All`     |
+-------+--------------------------------+
| 17    | :term:`Cloak of the Nightjack` |
+-------+--------------------------------+
| 18    | :term:`Curse of Creeping Doom` |
+-------+--------------------------------+
| 19    | :term:`Talent for the Unknown` |
+-------+--------------------------------+
| 20    | :term:`Greater Soul Portal`    |
+-------+--------------------------------+

.. glossary::

    Consumption
        Gain the :term:`Consume` supernatural ability

    Consume
        Supernatural Ability: As a standard action, you may make a touch attack against any living, sentient creature, to deal damage and gain health. The damage dealt and the health gained are each equal to one fourth of your Nightjack level, rounded up. This damage is untyped and bypasses all defenses. This ability has a 50% chance to activate on any successful melee attack against a suitable target. If the melee attack is a critical hit, double the effect of this ability as well.

    Curse of Twilight
        Gain the :term:`Curse` spell.

    Curse
        Spell: As a standard action, you may make a successful touch attack to apply a curse to a single target creature. That creature must make a Fort save against your `Save DC`_ or be `Slowed <slow_>`_ as though under the effect of the `Slow <slow_>`_ spell for 5 rounds. You must successfully :term:`Consume` at least twice before using this ability again.

    Consumption of Soul
        Your :term:`Consume` spell can now affect non-sentient living creatures. When using your :term:`Consume` ability as a touch action, you may choose not to inflict damage or gain health. If you do so, your target must make a Fortitude save against your `Save DC`_ or become `Shaken <shaken_>`_ for 2d4 rounds. You may only inflict a status effect in this way once per target per day.

    Cloak of Shadows
        Gain the :term:`Cloak` spell

    Cloak
        Spell: As a standard action you may wrap yourself in shadows, gaining 10ft increased move speed and the `Pounce <pounce_>`_ ability for a number of rounds equal to 2 plus one fourth of your Nightjack level, rounded down. Recharges on a short rest.

    Talent for Eating
        Double the contribution to your hit points from your constitution modifier.

    Soul Portal
        Gain the :term:`Soul Portal` spell: As a standard action, make a successful touch attack against a single target creature. If you hit, your body disappears and you live inside the soul of that creature until you decide to leave. You may not attack, use abilities or items, or take any other action during this effect, save to end the effect and leave the host creature. When you leave the host creature, you my choose to reappear in any square that is adjacent to the host. While under the effect of :term:`Soul Portal`, you can access the host creature's senses. The host gains additional hit points equal to twice your Nightjack level, 10ft bonus movement speed, their body is covered with obvious shadowy tendrils to mark your presence. The effect ends when you leave the host creature, or when the host creature dies. The host creature must be medium size and have fewer hit dice than you. If the difference in hit dice is 4 or greater, you take control of the host's movement and physical actions. You may draw weapons, manipulate objects, and perform attacks as the host, but you do not have access to any magic, special, or supernatural abilities posessed by the host and you cannot trigger magical items possessed by the host. Use your own skills when making physical skill checks through the host. This spell recharges after a long rest.

    Consumption of Essence
        Your :term:`Consume` spell can now affect sentient and non-sentient undead creatures. Whenever you successfully use your :term:`Consume` ability as a touch action, you may make a `Sleight of Hand <sleight of hand_>`_ check to immediately use :term:`Consume` again as a touch action against a new target. `Sleight of Hand <sleight of hand_>`_ check is made against the new target's `Spot <spot_>`_ check. You may not target the same creature more than once in any given round when using your :term:`Consume` ability as a touch action.

    Cloak of Starlight
        Your :term:`Cloak` ability now grants spell resistance of 30% and may be used as a swift action.

    Curse from Afar
        You may use your :term:`Curse` ability by making a ranged touch attack instead of a melee touch attack. The range is 30ft.

    Talent for Mayhem
        Gain an additional 10ft movement speed across all modes. This does not grant you access to any new movement modes.

    Eldritch Forces
        Any non-magical weapon you hold is treated as magical for the purposes of overcoming damage resistance and hitting incorporeal creatures.

    Theater
        When under the effect of :term:`Soul Portal`, if the host creature has at least 4 fewer hit dice than you, you may choose to make a :term:`Grand Entrance` when ending the effect.

    Grand Entrance
        Supernatural Ability: Only usable when under the effect of :term:`Soul Portal`. As a standard action, instantly kill your host, ending the effect of :term:`Soul Portal` and deals Force damage equal to 1d4 per Nightjack level to all creatures within a 20ft radius. All sentient creatures in view who are not in your party must make a Fortitude save against your `Save DC`_ or `Cower <cowering_>`_ in fear for 1d4 rounds. Additionally, while under the effect of :term:`Soul Portal` you are invisible to non-magical detection and cannot be affected by spells or magical effects, and you regain a number of hit points equal to your Nightjack level every round.

    Curse of Vulnerability
        Any creature that makes a successful attack (melee, ranged, touch, or ranged touch) against a target affected by your :term:`Curse` ability gains the effect of your :term:`Consume` ability on that attack. Unsuccessful attacks produce no effect. To recharge :term:`Curse`, you must now successfully :term:`Consume` at least five times instead of twice.

    Cloak of Infinity
        :term:`Cloak` now grants you immunity to all spells and magical effects and may be used as a free action.

    Talent for Survival
        Gain DR 5/adamandium

    Consumption of All
        When using :term:`Consume` as a touch action, instead of causing the target to be `Shaken <shaken_>`_, you may choose to inflict 1d6 points of Ability Damage to the Attribute of your choice. The target must make a fortitude save as normal, but the Ability Damage does not have an artificial duration and instead resolves in the normal way for Ability Damage.

    Cloak of the Nightjack
        When under the effect of :term:`Soul Portal`, instead of visibly exiting or making a :term:`Grand Entrance`, you may instead make a :term:`Silent Entrance`.

    Silent Entrance
        Supernatural Ability: Only usable when under the effect of :term:`Soul Portal`. When under the effect of :term:`Soul Portal`, you may use your :term:`Cloak` spell. Instead of its normal effects, you leave your host while becoming `invisible <invisibility_>`_ for the normal duration of your :term:`Cloak` spell. Viewers in visual range when you activate this ability get a Reflex save against your `Save DC`_. If they succeed, they perceive an effect around the host that clearly indicates your leaving, though they cannot see you directly.

    Curse of Creeping Doom
        Gain the :term:`Latent Curse` ability.

    Latent Curse
        Supernatural Ability: you may plant a latent version of your :term:`Curse` ability on a touched creature without the target knowing about it. To do so, make a `Sleight of Hand <sleight of hand_>`_ check against the target's `Spot <spot_>`_ check. If you succeed, your target gains a ``Latent Curse``. If you fail, the target has a 50% chance to know what you were trying to do. After doing this, you may not use this ability or your :term:`Curse` ability again until you have had a short rest. At any time, as a swift action, you may activate any ``Latent Curse`` you have inflicted on a creature, no matter where the target creature is. Doing so causes the target to immediately suffer the effects of your :term:`Curse` ability as if you had just used it.

    Talent for The Unknown
        Instead of 2 + 1/4 your Nightjack level, your :term:`Cloak` ability now lasts a number of rounds equal to your con mod + 1/4 your Nightjack level.

    Greater Soul Portal
        There are no longer size or hit-dice restrictions on what you can target with your :term:`Soul Portal` ability. Additionally, you may now use :term:`Soul Portal` as a swift action and with a ranged touch attack instead of a melee touch attack. Its range is 60ft.

.. _slow: https://www.d20srd.org/srd/spells/slow.htm
.. _pounce: https://www.d20srd.org/srd/specialAbilities.htm#pounce
.. _shaken: https://www.d20srd.org/srd/conditionSummary.htm#shaken
.. _sleight of hand: https://www.d20srd.org/srd/skills/sleightOfHand.htm
.. _spot: https://www.d20srd.org/srd/skills/spot.htm
.. _cowering: https://www.d20srd.org/srd/conditionSummary.htm#cowering
