What is this?
=============

D&D's character build and design system makes outdated assumptions about where fun comes from and about what modern players are looking for. Instead of giving players ways to make dynamic, creative, in-the-moment choices, D&D's character system forces much of the creativity to happen at build time, and then punishes creative decisions that don't make the same assumptions about character and party goals as those made by D&D's designers. I contend that this can be remedied without rebuilding the entire system, simply by rebuilding the class lineup.

Why Should I Listen to You?
===========================

.. image:: your-opinion.jpeg

I mean, you shouldn't. Everything I'm going to say here comes from my own experience; while I think I'm getting at something with fairly broad applicability here, it cannot perforce be universal. Thus, you should take everything I say and run it through the lens of your own experience with your own group and games. It may not apply for you, and that's fine. Hopefully I'll still give you some good ideas, but if not, that's fine too.

An Illustrative History in Three Characters
===========================================

I've been playing D&D off and on since the late nineties. I started (as many of us did) with my parents' AD&D books; but honestly tabletop roleplaying didn't really become a transformative experience for me until I was in college. Before then, it was mostly about satisfying teenage power fantasies, which is cool but not why most of us are really here. Though my experience is certanly not comprehensive, I've arrived at my current stance after sampling a lot of the variety that D&D offers and coming to some conclusions based on that breadth.

First Movement: Commissioner Gordon
-----------------------------------

My first character in college was also my first real departure from a set of common ideas that I'd been writing and playing with since I was fifteen or so. There were The Fallen Angel, The Lone Druid, The Mad Wizard, and a number of side characters, including:

* Beef, the smartest person in the world, and his weapon, an oar engraved with the word *KNOWLEDGE*.
* Beef's sometime companion, Steak, a half-orc enforcer with a penchant for quoting from philosophical texts.
* Gorty, the nigh-unkillable alcoholic gnome whose combat success came from his latent fighting instinct.

(I was a teenager; what did you expect?)

But in college I was invited to a D&D game with a couple friends, and instead of trying to bully the system into recreating one of the characters above, I looked around at the different base classes on offer by the system and I thought, "Everybody says Artificer is weird, so I'll do that!". And then the DM announced that because there were only three players, we would do a gestalt [#gestalt]_ game and everybody should pick a second class. I liked the theme and ideas behind the Knight [#knight]_, and so Max was born.

Max was a retired officer from some non-local military organization, and over the course of a 15-level campaign he evolved into a batman-style detective type; the party's frontliner and strategist, and head of a paramilitary investigative organization. When the party was given a choice to tackle any two of three possible objects and let the third fall to the wayside, it was Max who worked out a plan that allowed us to hit all three, taking a bigger risk with our available resources but with a reasonable enough chance of success that the rest of the party decided it was a risk worth taking.

I enjoyed Max as a character, but as character sheet he was atrocious for a couple of reasons:

* Bookkeeping for the Artificer class is just apalling; to effectively play the class requires an absurd amount of fictional time-management. It is based entirely on optimizing craft effects, so the idea is that with an Artificer around you can always get that obscure magical item that you need. This is fine for an NPC, but crafting a single magical item can take weeks of in-game time (years, if you're being ambitious). Sure, you can get custom wondrous items, but only if the world is willing to sit and wait while you create them. The artificer has some tools to mitigate these costs (you can have unseen servants to most of the work, for example) but you, the Artificer, still have to show up in your workshop on a regular basis in order to make the magic happen. For campaigns that grant the party lengthy periods of downtime, this is an excellent use of that time. But, come on, I'm playing a story, and epic conflicts don't just shut down for years at a time while the artificer crafts a bunch of shit. Instead, we had to be very selective about what we chose to craft, and as a result we ended up short on answers for some big fights, which ultimately got us killed and caused some unfortunate timey-wimey things to happen.

* The Knight class is just awful. Poorly designed and totally unbalanced (in a bad way), to the extent that the only things I got out of it past level five or so were the big hit die and proficiencies in heavy armor and martial weapons. *That's it*. The skill list is useless, the abilities (even the high-level abilities) are ineffective against all but the most pitiful of enemies, and the class provides absolutely nothing to make up for these deficiencies.

Those points can be boiled down into a couple of important goals for any system of character creation and development:

* When I make a choice about how my character functions, I shouldn't find out ten (or even two!) levels later that I unintentionally gimped my character. In most cases, it should be impossible to unintentionally gimp my character. This includes the steps involved in character generation! I should be able to throw a dart at the class list and have a reasonable expectation that what I pick will be roughly as effective as anything else I could have picked. Same for the race list, or the list of opening feats. A character system that forces me to memorize the entire system and perform feats of math and optimization at creation time *in order to not suck* is a bad system. D&D is a such a system.

* The amount of bookkeeping you have to do in order to manage your character should have no relationship to how effective your character is during the course of normal play. In general, I believe that bookkeeping is a scourge and should be minimized as much as possible, but I appreciate that I do not speak for all players in this, so I'm willing to take a step back from that and say this instead: I should be able to build a character based on whether or or not I enjoy bookkeeping, and have a reasonable assumption that my decisions in that regard will have neither positive nor negative implications about the expected effectiveness of my character relative to others in my party.

Second Movement: Songs of Self-Saucing
--------------------------------------

That group went through one large and a number of abortive campaigns in and after college. I expect this mirrors most people's long-term experiences with tabletop roleplaying; campaigns are more likely to peter out than they are to have a clean ending, for a thousand reasons that have little or nothing to do with the DM or players in question. Life happens and you deal with it.

My next notable example character was named Crawmerax [#craw]_; in a world oriented around ships and sailing, he was a hot-tempered fire-breathing, dragon-descended goblin with a trashy irish accent and a penchant for making fourth-wall-breaking jokes at the DM. I had been wanting to try out the Dragonfire Adept [#dfa]_, so I built a character that felt thematically appropriate to being able to constantly light everything in the immediate vicinity on fire. I was not dissappointed.

Like the Artificer, the DFA is optimized around a single game mechanic: The Breath Weapon. Unlike the artificer, the DFA was built with some actual thought toward player fun. In order to make the breath weapon work as a class feature, the designers made it an at-will ability for this class, and they provided for the character to grow by gaining a variety of different ways to modify their breath weapon. Then they went back and filled out deficiencies in the class by adding a small list of at-will invocations that the player could choose from as the character grew.

This made for an absolutely delightful play experience. Unlike an Artificer (or Wizard, or Sorcerer, or any other spell-oriented class), the DFA kind of only has one option for any given situation: use your breath weapon. Unlike the Knight (or Fighter, or Rogue, or basically any other non-spell-oriented class), the DFA can get actual mileage out of its core mechanic. Unlike the spell classes, a DFA's options are limited enough to encourage creative usage. Unlike the non-spell classes, a DFA's options are broadly applicable enough to actually *allow* for creative usage.

I breathed an ice bridge between two boats. I modified the battlefield by creating zones of frost and fire (and entangling weeds, and poison gas, and any number of other things). I set absolutely *everything* on fire, and I threatened to set even *more* stuff on fire, which sometimes got the party in trouble but was rarely not enjoyable.

This again can be boiled down to some important points for any class-based character system:

* Basing a class around a pre-existing game mechanic is a decision to approach with care and consideration. It is not enough to just throw together all the existing effects (feats, magic, etc) that use that mechanic and add a couple of unique abilities for effect. The mechanic in question and its existing uses probably weren't designed for that purpose, and you are likely to run into problems with things like dynamism and balance. Be prepared to use that mechanic in ways it wasn't originally intended, and to modify how it typically works in order to drive fun and interesting player decisions.

* Taking away the huge collection of specific situational options granted by a spell list is almost always a good idea, because spell lists fall victim to the paradox of choice. Instead, provide a simple mechanic that can be used repeatedly and submits to creative application, and then tell players to go forth and abuse the shit out of it. In general, class mechanics should seek to follow the principle of Limited Choice, Broad Applicability.

Third Movement: The Invisible Hand Full of Invisible Sharp Things
-----------------------------------------------------------------

Some time after that, I rolled up a character called Pentax. At this point, I'd seen first- or second-hand basically everything the system has to offer, and the available class list just wasn't satisfying, so with our DM's permission I decided to go looking for decent homebrew content. Unsurprisingly, there's a bunch out there. Also unsurprisingly, built mostly in terms of forum posts in places like MinMax Boards and Giant in the Playground, it's not terribly well organized. I eventually stumbled on `Grod The Giant's Fixed List Caster Project <https://forums.giantitp.com/showthread.php?317861-Fixed-List-Caster-Project-(3-5)>`_, and settled on the Mage of the Unseen Order contained there.

I found it to be flexible and sustainable enough to work but not nearly so satisfying to play as the DFA above. Some points which stand out to me:

* The spell list was both arbitrary and capricious. I don't blame Grod or their class for this; I've looked through list after list of D&D spells, and you know what? The whole thing, taken in aggregate, is still arbitrary and capricious. I'm not against using something arbitrary and capricious in the small, but basing  the entire game on it? Nuh uh.

* Vancian spellcasting mechanics (that whole, I've got 5 sixth level spells per day, and 4 seventh level spells per day, etc) is, like, dumb. Games should reward dynamic decision-making, and this doesn't reward dynamic decision-making, it just punishes you for making mistakes in your character plan, either when (as a prepared caster) choosing your spells for the day, or (as a sponaneous caster) choosing which spells to gain as you level, or (during the normal course of play) casting through some subset of your spell list without realizing (because, you know, you can't see the future) that those spells will be more useful if you save them for the bigger thing the party will fight later that day. As with an arbitrary and capricious mechanic, I'm all for adding stakes and time-costs to things, but using the same system of stakes and time-costs for everything? No, that's dumb. Vancian magic is dumb.

* As with the Artificer above, Mage of the Unseen Order turned out to be a grab-bag of class abilities that were unified by a theme but were mechanically unrelated to one another. The biggest example of this is that as a Mage of the Unseen Hand, you probably have something like three telekinetic arms, all invisible, each with a reach of thirty to forty feet, which you can deploy at will to manipulate and move stuff. Sounds cool, right? But you'll never use them. I used them to plant and hold shields in front of other party members *and that's about it*, because if I did much more than that, I would have been waving my fabulous arms around when I could have been casting spells at things. I could use telekinesis at will as a supernatural ability, but as with my numerous arms, it almost always ended up being a waste of my time.

My last set of takeaways, I promise, and then I'll build into my unifying guidelines for a character system:

* There are two ways to provide flexibility. One is to provide flexible mechanics. The other is to provide a wide variety of very specific mechanics. D&D chose the latter, that choice makes D&D a bad system. Instead, systems for building characters should almost always strive to produce characters whose abilities are limited in choice (i.e. I have one or two options to choose from in any given situation) but broad in applicability (i.e. My two options are very rarely not useful, and I can apply them in a variety of different ways or circumstances to produce interesting effects).

* If you have a system that is arbitrary, in that it can be used for some things but not others *for the sole reason* that you, the designer, say so (i.e. you can have *these spells* and no others!), use that system sparingly. Build one class off it, or a couple of monsters, not the entire game.

* If you have a system that rewards good decision making not by granting rewards but by *failing to impose penalties* (i.e. you didn't choose the right spell when levelling up, or you didn't put the right selection in your spell book), use that system sparingly. Rewards are almost always what players want. Penalties are frustrating and make us feel stupid, and if we feel frustrated and stupid all the time, we aren't having fun.

Summarizing the Above
=====================

As a rule, I enjoy playing D&D. But more and more, I enjoy D&D in spite of D&D's core systems of character development. Levelling is a chore, because the game's encounter and monster design are built with the assumption that I will optimize, and optimization A) takes time and effort, B) is dull and arbitrary. Spell and Feat selection is a chore, because the lists are arbitrary and never have the effects I want. Shopping is a chore and shouldn't be mandatory because it's duller than dirt, but it *is* mandatory because of the above assumptions regarding optimisation. Session to session play is annoying because of all the time we collectively have to spend looking up definitions and errata for our vast selections of highly specific effects. I routinely find myself wanting the system to just get the hell out of the way so that I can do the thing I'm trying to do without having to pay a sanity tax in order to make it happen. I lay all of this at the feet of bad design, specifically bad design in the character system, along the following axes.

Choosy Choosers Choose Later
----------------------------

In standard D&D, the most important choices you make, from the perspective of the system, are at character creation and level-up time, because those choices are what determine your available options at play time. Now, this is not in and of itself bad; we all play games that require us to put some forethought into character design, *because* we want to put forethought into character design. The problem is that if you make the wrong decisions here, this game will punish you, and punish you hard, and it will not let up, and your only recourse will be to roll a new character and do better next time. This means that in many ways and in many sessions the only decisions that in fact matter will be decisions that the players made days, weeks, even months ago the last time they made a decision about character growth. Did you choose to play Fighter? Get ready to discover at level 10 that you have little to do in your party, and instead the wizard is basically doing your job for you. It does not matter what feats you take. It does not matter what weapons you hold. A Fighter in a party with at least one spellcaster will sooner or later become redundant.

Additionally, in a standard game of D&D, with a few notable exceptions, player choices in game are severely curtailed. If we look at the magic system, the player gets access to a pre-defined list of spells, most of which are highly specific, and the player cannot go outside of that list in order to perform magic, except by buying items that for the most part recreate other spells on the spell list. I mentioned above some of the ways that this is bad design, so I won't rehash it in great depth here, but I will reiterate that having a bazillion spells available is actually far more limiting than it seems. We are at our best when improvising within guardrails, not when selecting from a list, even (sometimes especially) if the list is huge. Forcing us to rely on a list of specific possible actions dampens our creativity and makes the game dull.

So how do we fix this? By reducing the quantity and variety of available options and instead providing a very limited selection of options, each of which can be applied in a lot of different ways and across a lot of different circumstances. Limited Choice, Broad Applicability. The goal should always *always* **always** be to provide the most interesting choices at play time, because play time is when the choices matter. Play time is when we play. Character creation time is a tax that we pay in order to get to play time, and while we can tolerate some choices at character creation and growth time in order to facilitate better play time, the choices at play time are the ones we're all here to experience. Choosing an archetype that will have downstream effects to the rest of your character growth: Good choice. Choosing a spells to add to your spell list (or feats to your feat list) from a thousand possible options: Bad choice.

Scaling and Its Consequences
----------------------------

The existing character system is absolutely chock full of spells and abilities that are only useful at or below certain levels. This is fine, if we can put those things in items. It is not fine as a character option. Levelling out of a class ability is like buying a car with a sticker on it that says "The battery on this car will fail after ten thousand miles. Don't worry! This is intentional and everything is fine! You can buy a new one!". It's not fine! Why did you build it with such a terrible battery? Indeed, when designing class abilities and growth charts, we should craft every class option with the expectation that it should remain useful from the time it is first acquired until the character is retired from play. This will not always be doable, but keeping it as a prominent goal encourages us to design more robust and well-rounded abilities, which in turn provides players with more robust and well-rounded options at character creation and growth time.

This has implications! This means that it is no longer feasible to just throw out a huge variety of half-baked options and hope that some of them are useful to players. Instead, as designers we will be better served by putting our effort into designing a smaller variety of balanced, scalable class features that players can use continually. So, when concieving of class ideas, pick your battles; if your idea will require you to write a hundred new spells, maybe revise your idea to be less demanding.

Time and The Action Economy
---------------------------

What does a Fighter do with their action? They hit something. If the target doesn't really care about getting hit, as tends to start happening at around lvl 12 or so, the Fighter looks for a different target, but if they can't find one, the Fighter hits the thing, with the idea that eventually we'll tire it out, or something. What does a Wizard do with their action? They pore over their spellbook for an hour and then cast ``Limited Wish`` or some other bullshit that ends the encounter. The Fighter got much less done, but there was a lot less side-conversation during the Fighter's turn than during the Wizard's, because it's fun to watch somebody describe an action and roll dice, but it is dreadfully dull to watch the Wizard and the DM debate over spell mechanics. The problem is, the Fighter didn't actually get anything done, and the Wizard ended the encounter.

There are a lot of reasons this happens, but primarily this goes back to a point that I've reiterated a couple of times now. D&D spells are bad. Not because they don't accomplish goals (they obviously do that, if you've got the right ones available), but because they actively discourage both creativity and party interaction. Instead, it's just the Wizard poring over their spell book while the rest of us chat about the weather. I've been on both sides of this dynamic, and it's no better being the Wizard; people chatting about the weather can be very distracting!

Now, you'd think there would be some kind of middle ground here, but with the exception of the Dragonfire Adept described above, there really isn't. Either you have one option, and it's terrible, or you have a bazillion options, and one of them might be good, but you won't know until you've read them all because god dammit I have a day job and I can't just memorize spellbooks on demand. This isn't because a middle ground cannot exist. It's because the designers of D&D have continually doubled down on spells to provide all the character options, so if you don't have spells, you don't have options, and if you do have spells, well, as we've discussed, spells are bad.

So what should we do instead? In short, take the D&D archetypes and throw them into the garbage. Fighters should have access to magical effects. Rogues should have access to magical effects. Wizards should *not* have access to all possible magical effects. Basically, throw out spells. And feats. Hell, throw out wizards and sorcerers and clerics and fighters and rogues and replace them with classes that are actually interesting.

Specifically, design a class with an eye toward what kinds of situations that class can own every time, and make sure there there are at least a couple but not very many. Then add to that class such that in most other situations a player has ways they can contribute, by changing the landscape or disctracting other participants or *something* so we aren't all just sitting around listening to the party Face negotiate with the stupid Mayor about our pay. This is easier than it sounds, but it involves throwing out standard D&D assumptions about what characters should be doing what. It's the 21st century, folks! We can mix it up a bit!

I don't entirely *blame* the designers of D&D for these poor design choices; when you're working with existing IP, imposing radical change is difficult, as we all saw with 4th Edition. After failing in such a task once, I don't blame them at all for taking a conservative stance and going back to what the D&D community is already familiar with. Nevertheless, what we are familiar with remains a badly designed and inaccessible system.

I, however, am not them and I'm not beholden to their interests, so I can do whatever the hell I want, and what I want is to take a stab at rebuilding the character system from scratch, starting with the class list. I may do more than the class list, but I believe that fixing class list on its own will be enough to inject a significant amount of new fun into existing D&D games, so the class list is where I intend to start.

So, How is This Going to Go, Exactly?
=====================================

I'm going to start by distilling all of the above into a set of guidelines that is as quick and easy as I can make it, and then I'm going to provide an example that I think satisfies those guidelines and explain how and why.

Designing a Class System for The Now Times
------------------------------------------

A class designed for fun, and not just as a slavish homage to previous editions of D&D, should in general satisfy the following guidelines:

* The class should be really good at one or two things, and have a hard time being really good at anything else.
* The class should explicitly provide a mix of in-combat and out-of-combat abilities, as well as a mix of social and non-social abilities. No class should ever be completely lacking in any of those four arenas.
* The class should have one core mechanic, two or three active abilities, and one "passive" ability that applies at all times. The character should earn these very early in their career, and all development from that point forward should either add options to or provide improvements on those abilities, or on the core mechanic itself.
* The class should have at most three points at which the player must choose one variant of the class or another.
* The class should use no spell lists and assume no feats or items in order to satisfy its core mechanic.
* The class should have no dead levels.

For example: `The Nightjack <classes/fighter/nightjack>`_

A rundown of how The Nightjack satisfies the above rubrick:

Do One or Two Things Well
*************************

The Nightjack does a couple of things well:

* With easy access to both ongoing self-heal and temporary magic immunity, the Nightjack is highly survivable. Nightjack players should expect to implement extremely aggressive combat tactics to throw enemies off balance, trusting that judicious use of their abilities will enable them to survive even the most hairy situations with limited support from the rest of the party.

* The ``Curse`` ability makes the Nightjack an excellent force multiplier for parties trying to face down a large, difficult target. Because the ``Curse`` ability has the potential to grant healing to the rest of the party, a Nightjack player with good timing should expect to be able to singlehandedly save their party from many situations that would otherwise kill them.

The Nightjack does most other things poorly:

* With a very limited skill list and no incentive to invest in Charisma, a Nightjack will make a poor party Face. The Nightjack can contribute during social situations by stealthily inflicting status effects on unsuspecting targets, but they should generally not be acting as a spokesperson for the party. This is probably for the best, because the Nightjack's highly aggressive play style would probably get the party into more trouble than it gets them out of.

* With a mostly melee-oriented skill set and no built-in extra movement modes, the Nightjack will have a hard time dealing with any enemy they cannot close with, especially those that fly. In such situations, the Nightjack should look to other members of their party to either tie such foes down or help the Nightjack get into a more aggressive position.

* The Nightjack's ability to buff, heal, or otherwise directly support the rest of the party is severely limited. In general, a Nightjack player will have a hard time coming to the aid of their party unless there is something nearby to attack, and even then extra hit-points are often not what is called for. Nightjack players should look to other members of their party to provide things like:

    * Resistance to or removal of status effects
    * Healing outside of combat
    * Mass buffs and debuffs

* Though the Nightjack is highly likely to survive such an encounter, they will have a hard time assisting their party against large groups of enemies, beyond slaying the enemies one at a time. The ability to string together touch attacks and sleight of hand checks to spread status effects quickly throughout grouped enemies is likely to be of utility but expect that utility to be limited by of both the associated restrictions and the underlying utility of the status effect in question.

Cover Your Combat/Non-Combat Bases
**********************************

The combat options for a Nightjack player are pretty straightforward, so I won't go over them in detail here. Suffice to say that the Nightjack is good at hitting people, and we can move on. In a classic D&D design, this class wouldn't have any explicit out-of-combat utility; players would be expected to make that happen through skill checks and creative dialogue.

The Nightjack, however, explicitly has some out-of-combat options. Beyond just generally being threatening much of the time, a Nightjack player can:

* Use touch attacks to inflict socially debilitating status effects (``Sickened`` or Ability Damage). By chaining touch attacks and sleight of hand checks, a Nightjack should have a reasonable time (for example) making it look like everybody in the town hall has come down with the plague.

* Use ``Soul Portal`` for......lots of things. Riding around in somebody's head without them knowing you're there can be incredibly useful. Some suggestions:

    * Catch (or put) your enemies in compromising positions.
    * Listen in on conversations people might not want you to hear.
    * Cause people to explode in the middle of an otherwise uneventful meeting, because you are a Nightjack and you got bored.

Limited Selection, Broad Applicability
**************************************

Becoming magic-immune as a free action is useful everywhere, not just in combat, and so is inflicting status effects or causing nearby creatures to heal when damaging a given target. Sleight-of-handed touch attacks can be used for any number of different things, and let's not even get started on ``Soul Portal``. But at any given time, the Nightjack will almost always be deciding *how to use an ability*, not *which ability to use*. Indeed, the Nightjack never really has more than four choices:

* Hit something
* Cloak
* Curse
* Soul Portal

And in most situations at least one of those will be on cooldown and it is likely to be quite obvious that one or two others don't make sense. Thus, for any given situation, the Nightjack player is likely to spend a lot more time looking for creative ways to apply the things they have, instead of checking to see if they have the right thing.

Limited Choices at Creation and Growth Time
*******************************************

In fact, as of this writing, there are none, though as this is an early draft I expect that to change.

No Item, Spell, or Feat Dependence
**********************************

You do not need a Belt of Ogre Strength in order for this class to be viable. In fact, you should be able to play a Nightjack who runs around in the buff without too much trouble. You'll take more hits and deal less damage, certainly, but your self-heal should still make this at least plausible instead of *just* a terrible idea. Just, make sure you ask your party first, because they might not want to see you perform a ``Grand Entrance`` with your bits hanging out.

Likewise, the Nightjack's relationship to spells is more of a shrug than a need. The Nightjack can certainly benefit from well-timed buffs and heals (remember, magic-immunity during ``Cloak`` cuts both ways), but there is no single spell that the class depends on, and as we mentioned above a Nightjack player can often expect to survive highly aggressive moves with little to no support from the rest of their party.

With feats, too, you *might* want to take Power Attack, but you'll be fine without it. Chances are you might not be the big damage dealer in the group, so as long as you can at least keep up, that's a feat you can spend on something you find more interesting.

Ultimately, you shouldn't really need to optimize this class for it to be workable at all levels. It shouldn't dominate the table, but it should never just be the party slouch, and you should very rarely have no options when playing a Nightjack.

No Dead Levels
**************

Check. I'm not entirely satisfied with all the level gains, and probably the order could use some work, but I expect that many players who are fed up with the standard class list will see this as a breath of fresh air, wacky level distribution and all.

But, Well, This All Seems Pretty Specific...How Do I Build My Vision?
---------------------------------------------------------------------

D&D's character system is built around this idea that we (the designers) should try to support players in building basically whatever they want, and this is absolutely the right goal. But the way D&D chose to do this was to provide a fixed and limited class list, and to then try account for customization by providing lots of ways to modify each class at build time. This could have worked! But instead of putting the work in to properly balance all the options against one another and put some guardrails up so that players have to actually *work* to shoot themselves in the feet, the designers of D&D have traditionally phoned it in when it comes to actually *providing* the options they had planned for players to have. As a result, foot-bullets are the default outcome when building characters in D&D and you, the player have to put work in at character creation and growth time to make sure that doesn't happen. That is backwards and frustrating.

By providing classes that are tightly limited in scope, I answer the problem of guardrails; you should be able to pick up any class in this list and kind of handle it however you feel like, and the game should no longer actively punish you for that decision. But there's a tradeoff here, which is that because these classes are so tightly built, they don't really flex very much at build time. When you level up as a Nightjack, there aren't really any decisions for you to make, and if you want to build a character and your vision of your character differs slightly from what the Nightjack provides, you are kind of S.O.L.

So, how to resolve this? I think the designers of D&D originally wanted players to be whipping out new Feats, Spells, and Items left and right to suit their needs. But the problem is, how do you know your Feat is balanced? Well, the only way to really balance a Feat is against all other possible Feats, and that's... even the designers *themselves* didn't bother doing that, so how can you expect us to? As a result, homebrew feats aren't really a thing, because almost anything you build, somebody else can trivially come along and break by pairing it up with some *other* homebrew feat. Or just by doing something you didn't expect. But, you know, there are quite a lot of homebrew base classes out there. People are chucking out homebrew base classes or modifications to existing base classes all the time. Are they all balanced? Hell no. But people are still building them anyway.

I think there's a reason for this. I think that in spite of being much larger in scope, classes submit much more readily to building and balancing because they all pretty much follow the same logical structure. See my guidelines above, for example. What guidelines would you provide for a Feat? ``Don't instantly break the world.`` is about as good as it gets; because Feats were intended to be a one-stop shop for character customization options, it became impossible to put real limits on what a Feat could and could not do, beyond the truly obvious or mechanically absurd.

So, I propose to resolve the question of giving players the ability to build exactly what they want by doing just that: letting them build exactly what they want. I don't plan to write very many new feats, or spells, but I do plan to write new classes, and I want players to be able to do so as well; instead of providing growth-time customization options, what I want to do instead is to provide a set of guidelines (like the ones above) and quick heuristics by which a couple of things can happen:

* Players with limited or no design experience should be able to work with their DM to *quickly* build exactly the class they want, either from scratch or from existing parts. Indeed, building a new class from scratch should be a viable option for most players in most games.

* DM's should be able to look at a homebrew class description and *quickly* get a  rough sense for how that class will balance in their game.

I don't entirely have those heuristics nailed down yet, but I think the guidelines above are a pretty solid beginning. They give you a quick place to start: one core mechanic, one passive, three actives, each to expand and develop over time at different rates. And they give you narrow guardrails; as a DM, the balancing points of the class should in most cases be much easier to see at a glance, and because the class doesn't have a spell list with a bazillion entries, you can be reasonably sure there's nothing hiding out of your view.

Additionally, I propose to provide play-time options to change how your class works in the form of items. These changes may be large or small, minimal to fundamental, but they should all be roughly even changes (and not buffs, nerfs, or expansions). The idea is, if you don't like a particular feature of your class, go on a quest for an item that lets you swap that feature for something else. Why do it this way? Because items are fungible. Don't like it? Sell it. Want something else? Buy it. This means the cost of making a choice you later regret is counted in terms of in-game gold instead of out-of-game time rolling up a new character. Plus, it means you can keep a couple of builds handy for different purposes, if that's a thing you like to do.

Wrapping Up
===========

I hope that what you've read so far sounds fun and interesting to you. It sure does to me. As of this writing, I've only just started, which is why if you look in the class list you will only see the one. If you're a video gamer, you might have noticed that a lot of my mechanics are ripped straight from DotA 2. There's a reason for this! I think DotA 2 (and the MOBA genre in general) have a lot to offer in terms of the kinds of goals I set out above; the characters are obsessively balanced against one another, and each is purpose-built to be *part of a team*. D&D is a team game, but its classes all seem to have little to no synergy with eachother (unless you count the various buff spells, but you shouldn't because for the thousandth time, D&D spells are bad). So, instead of building an entire new class list from scratch, which would take a lot more time and effort, I plan to steal shamelessly from DotA2 and other MOBA games and make minor updates to suit turn-based vs. real-time and non-combat vs. combat-only play. Nightjack is an almost verbatim ripoff of DotA2's `Lifestealer <https://dota2.gamepedia.com/Lifestealer>`_. I've chosen to do it this way for two reasons:

* As I mentioned above, the characters are all kind of pre-balanced; if I can do the lift-and-shift with minimal alterations, differences caused by game type or just my own judgement calls should mostly shake out in the wash. Even if not, it's not like the bar for balance is particularly high here; D&D's character classes are famous for being unbalanced piles of steaming garbage.

* As a matter of opinion, I generally find DotA2's characters to be both thematically and mechanically interesting. The idea that each class should kind of have its own core mechanic is compelling to me, because it means that we can (in theory) facilitate a far wider variety of play styles than the current philosophy of "Everything is the same and you customize with feats and spells." This should lead to greater dynamism and more interesting play in general. Plus, most of them fit into the guidelines laid out above with little additonal work, because I'm not just cribbing characters from DotA2, I'm also cribbing design theory.

My immediate goals are:

* I want nine base classes to start with, three Str- or Con-based, three Dex- or Cha-based, and three Int- or Wis- based. When putting these together, I want to shoot for a variety of different roles, positions, and play styles when doing so, in order to demonstrate exactly how this system works. Not all the characters in DotA2 map well onto the guidelines above, so we should be careful to start by picking those that do.

* Nothing should change outside the class list; all other existing rules should be left alone. Players and DM's should be able to use this class list as a drop-in replacement for (or in addition to!) the existing D&D base classes with zero hassle.

* Theming and fluff should be kept to a minimum; obviously things need names, but we should not be opening our class descriptions with lengthy, thematic prose  to introduce our classes. Those are for players to provide, and we should by and large just stay out of the way and provide the mechanics.

* Ideally, classes should come with some suggestions to players and DM's to how they might alter it in order to fit different ideas and scenarios. One of the core ideas of this system is that Players and DM's should be participants in class design, and not just consumers of it, and we should strive to provide resources that make that both clear and easy.

* From there, I want to build out into modifier items (by which players can switch out class options) and to pull from DotA2 some of the characters whose design is a little more experimental under this system. The guidelines above are intended to be guidelines, and we should demonstrate what it looks like to safely break one or more of them in a given class.

This Sounds Great! How Can I Help?
==================================

Glad you asked! The project is maintained on gitlab at https://gitlab.com/druidgreeneyes/dnd-homebrew, so feel free to raise an issue or open a pull request. I'll respond as and when I have time.

.. rubric:: Footnotes

.. [#gestalt] Gestalt is a D&D 3.5 mechanic whereby players pick two classes and smash them together to create characters that are, on average, signifigantly stronger than non-gestalt characters.

.. [#knight] Knight was an attempt at a WoW-style tank, with abilities aimed at drawing aggro and protecting nearby party-members from harm.

.. [#craw] After the Borderlands raid boss, because, come on, what a great name.

.. [#dfa] DFA was released in the Dragon Magic sourcebook, along with a number of fun optional modifiers to existing classes. It was built around a single mechanic: the breath weapon.
