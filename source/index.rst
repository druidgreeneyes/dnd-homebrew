Overview
~~~~~~~~

Here we set out to create a replacement base class list for D&D 3.5, intended to satisfy a couple of goals:

* Play with these classes should be fun and dynamic; they should support and reward creative decision-making at play-time rather than at build-time.

* Though different classes should excel in different scenarios, each class should be evenly balanced against the rest.

* Each class should have an interesting mix of abilities for different situations, including:
    * in-combat vs. out-of-combat
    * social vs. non-social

The ultimate goal is to increase fluidity, dynamism, and (ultimately) fun at play-time. For the full rationale behind this work, go :doc:`here </rationale>`.

Classes
~~~~~~~

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    classes/fighter
    classes/rogue
    classes/wizard

Mechanics
~~~~~~~~~

In order to support the classes above, we have made some small changes to character mechanics.

Attributes
==========

* Strength now provides bonus attack damage but NOT bonus hit chance. Classes that gain hit chance from an attribute list that attribute in the class description. All attack damage bonuses now come from Stength; strong characters wield heavier ranged weapons just as they hit harder with melee weapons. Additionally, all classes how have full BaB unless otherwise specified in the class description.
* Dexterity now provides extra attacks but NOT bonus AC. Classes that gain armor from an attribute list that attribute in the class description. In addition to bonus attacks granted by BaB, characters gain one bonus attack for every 2 points of their dexterity modifier.
* Intelligence now provides extra crit chance and crit damage but NOT extra skill points. Class skill point allocations have been increased across the board to account for this. Characters gain additional critical threat range for every point of their intelligence modifier (so a character with int mod +1 wielding a weapon with crit range 19-20 has a chance to crit on rolls from 18-20), and critical damage multiplier for every two points of their intelligence modifier. This crit damage multiplier is additive with respect to other multipliers: if a character with int mod +2 wields a weapon with crit damage x3, that character's crit damage is dealt at x4, not at x6. Likewise, the same character with int mod +4 would deal crit damage at x5, etc.

This is Cool! How Can I Help?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Glad you asked! The project is maintained on gitlab at https://gitlab.com/druidgreeneyes/dnd-homebrew, so feel free to raise an issue or open a pull request. I'll respond as and when I have time.

My initial objectives are to produce a list of nine base classes, three in each of the three "core" chassis: Fighter, Wizard, and Rogue. In order to save on development effort, I intend to steal classes wholesale from characters in MOBA games, most notably DotA2. I assume that doing this will require minor mechanical adjustments (see the list above), but my goal is that those should be minimized as much as possible while still preserving the play-style of the character that we're porting.

As such, the immediate needs for the project are:

* Fill out the rest of the class list so that we have three classes on each chassis.
* Playtest drafted classes in two contexts:

    * Contexts in which the entire party is composed of classes from this list.
    * Context in which only some of the party is composed of classes from this list.

  We're looking to find out about a couple of things:

    * How easy is it to roll up a character from this class?
    * Does the character balance pretty cleanly within `Tier 3 <http://minmaxforum.com/index.php?topic=658>`_? Specifically, we should be aiming for the first qualifier of Tier 3: "Capable of doing one thing quite well, while still being useful when that one thing is inappropriate. Occasionally has a mechanical ability that can solve an encounter, but this is relatively rare and easy to deal with. Challenging such a character takes some thought from the DM, but isn't too difficult."
    * Does the character support creative, dynamic play? (That is, does the player find themselves using one or a couple of their abilities in a wide variety of different circumstances and ways, or are they constrained to specific scenarios?)
    * Does the character synergize well with the rest of the party?
    * Does the character have options both in and out of combat, both in and out of social situations?

More specifically, here's how this works:

* You will need to know how to use `git <https://git-scm.com/>`_ in order to work on this repository. It's not hard; it's also not optional.

* All the content is in reStructuredText markup language, and we use `sphinxdoc <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_ to translate it into a static webpage that is hosted on gitlab pages. This happens automatically every time a change happens to the ``master`` branch. We advise using the existing class documents as templates.

* If you have a draft of a new request, please make sure you have that class in a branch of this repository; we clean branches semi-regularly, but please try to choose a branch name that is both informative and unique, like ``new-class/your-class-name`` (for example: ``new-class/forgemaster``). Branch names should be lowercase only. Once you have a branch and your branch is up to date, you can use the gitlab UI to create a pull request from your branch to master, at which point we will take a look at your work and either approve your PR or ask for updates. Some of the basics that we will look for include:

    * A chassis
    * A short description of the intended play style (see existing classes for examples)
    * Provenance (i.e. if your class is a port of a DotA2 character or something from another game, we need to know that)
    * Move Speed
    * Class Skill List
    * Skill Points per Level
    * The class level ability table
    * Complete descriptions of all abilities

  If we have other questions or comments about the content or structure of your class, we will point those out in the pull-request discussion thread.

* If you have an improvement to an existing class, please make sure you have that work in a branch of this repository; we clean branches semi-regularly, but please try to choose a branch name that is both informative and unique, like ``the-class-name/the-fix`` (for example: ``nightjack/fix-soul-portal-scaling``). Once you have a branch and your branch is up to date, you can use the gitlab UI to create a pull request from your branch to master, at which point we will take a look at your work and either approve your PR or ask for updates.

* If you have play-tested with a given class and wish to document your experience, please raise an `issue`_ in order to do so.

* If you have discovered a problem with a given class or with the system in general, please raise an `issue`_ and provide a clear description of the problem.

In any and all cases, we will respond as and when we have time. Everybody who works on this does so in their free time and for zero pay, so please be considerate and supportive in your comments and discussions.

.. _issue: https://gitlab.com/druidgreeneyes/dnd-homebrew/-/issues/new