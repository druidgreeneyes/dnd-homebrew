Title: On Dungeons and Dragons [WIP]
Status: published

<!-- toc -->

<!--

- Thesis: D&D would be more fun with better and more varied character mechanics
- An illustrative history in three characters
  - Max (Artificer/Knight gestalt)
    - Wanted to be the best at everything (didn't realize it at the time)
    - Fun but mostly useless
      - Time dependence (didn't quite realise this going in)
      - Wands/umd are annoying
      - Knight is just terrible
      - Bookkeeping was frustrating
    - Character aspects were fun, mechanics didn't work.
  - Crawmerax (Dragonfire Adept)
    - Delightful to play
      - Limited abilities, used at will
      - Highly survivable (except for the time we died)
      - Easy to manage
    - Best of the lot
  - Pentax
    - Homebrew class modelled after Beguiler
    - Strong and flexible, but a bit dull to play
      - Vancian spells are stupid
      - Spell lists are by definition arbitrary and capricious
      - Grab bag of class features was difficult to find applications for
    - Eminently playable, but lacked verve
  - Thoughts after all these years
    - The hazy relationship between tradition and fun
      - build vs. play
      - on the mechanics of markets
      - Vancian magic is fucking stupid
    - The bones of a good system are in here somewhere, buried under a morass of terrible character design choices.
- Considering a Refactor
  - Change should be minimal
  - Adaptation should be easy
  - Changes should be friendly to piecemeal use
- So what do?
  - Scrap the class list and leave everything else alone
  - Mechanical goals
    - Limited variety, broad applicability
      - Rewards creative uses instead of list memorization
      - Easier to keep your character in your head, less reliant on accurate bookkeeping
    - Use existing mechanics in new ways
      - Dynamism and imagination over detailed character planning
      - Examples
        - Grapple
        - Changing equipment could enable on-the-fly changes in play style
        - Spells might be rare but potent or common but expensive or weak but spammy
    - Scaling
      - Everything you get at level 1 should still be useful at 20, or the class should have replacements built in.
    - Tactics
      - Coordination should be key
      - No class should be able to one-shot every encounter
      - No class should ever have nothing to do
      - It should be easy to build a well-rounded party without anybody going "Aww, man, I don't *want* to be the stupid healer."
    - In-game mechanics for using ad-hoc effects in
      - Dynamic spell creation
      - Dynamic class creation
      - Dynamic item creation
  - Leave everything else alone.......mostly
    - A few spells might change in small ways
    - Basic equipment might change in small ways
    - Monsters should be left alone
  - Balance
    - Coordination
    - Optimization
    - Edge Cases and How we deal with them
- The new class list
  - Overview
    - Modelled on Dota2/MOBA games
    - Three archetypes
      - Fighter (High Str/Con)
      - Wizard (High Int/Wis)
      - Rogue (High Dex/Cha)
    - Each archetype becomes a chassis
      - Fighter - full bab, strong fort save, d12 hit die
        - This provides a baseline for characters with high survivability against damage but weakness to magic effects
      - Rogue - 2/3 bab, strong ref save, d8 hit die
        - This provides a baseline for highly mobile characters, with enough survivability for mid-range or hit-and-run tactics and resistance to aoe effects.
      - Wizard - 1/2 bab, strong will save, d4 hit die
        - This provides a 
    - Building classes on a chassis
      - 4 distinct abilities
        - Passive
          - Constant effects on or produced by the character
            - Defensive or offensive buffs
            - Defensive or offensive debuffs
            - Localized environmental effects
            - Special cost mechanics
            - Attack modifiers
        - Active
          - Relatively low cost
          - Relatively limited effect
          - Should work out to once or twice per encounter
          - Functions:
            - For attackers, supplements or enhances attacks
            - For controllers, provides the primary control mechanism
        - Escape
          - Medium cost
          - Should work out to once every couple of encounters
          - Effect should usually provide an escape mechanism in sticky situations
            - Move the character
            - Grant extra mobility
            - Move allies
            - Move enemies
            - Disable enemies
          - Functions:
            - For initiators, gets the jump on enemies and provides surprise mechanics
            - For squishy characters, provides emergency survivability
        - Capstone
          - High cost
          - Should end up being saved for boss fights
          - Effect should either be powerful on its own right, or should change the local game such that the character's other abilities make them powerful
      - Capstone at 6
      - The other three spread out in arbitrary order from 1-5
      - Each ability grows in three increments, each of which should in some way change how the ability can be used, either by adding additional effects or by providing additional options
      - Core mechanics should scale automatically in most circumstances (i.e. dmg/duration/effect by level or some other scaling modifier instead of at a flat rate)
      - Don't forget about out-of-combat situations!
        - Include options for non-combat effects
        - Pick effects that overlap between in/out of combat
-->

# The Classes

## Fighter

These will be classes that primarily focus on strength or constitution for their primary effects. They will almost all have high hit-points and high survivability, but they may lack mobility or ways to interact with a target at range. They build on the Fighter chassis, which has:

- Full BaB
- Strong Fort Save, weak Will and Reflex saves
- d10 hit die

### Nightjack

Chassis: Fighter

Move Speed: 35

Skills:
- Climb
- Intimidate
- Jump
- Swim
- Sleight of Hand

Skill Points: 2 + Int Mod

| Level | Gain | Description |
|-|-|-|
| 1 | Consumption of Mind | Gain the `Consume` supernatural ability: As a standard action, you may make a touch attack against any living, sentient creature, to deal damage and gain health. The damage dealt and the health gained are each equal to one fourth of your Nightjack level, rounded up. This damage is untyped and bypasses all defenses. This ability has a 50% chance to activate on any successful melee attack against a suitable target. If the melee attack is a critical hit, double the effect of this ability as well. |
| 2 | Curse of Twilight | Gain the `Curse` supernatural ability: As a standard action, you may make a successful touch attack to apply a curse to a single target creature. If the target fails its Fort save (DC 10 + 1/2 your Nightjack level + your con mod) it is `Slowed` as though under the effect of the `Slow` spell for 5 rounds. You must successfully `Consume` at least twice before using this ability again. |
| 3 | Consumption of Soul | Your `Consume` ability can now affect non-sentient living creatures. When using your `Consume` ability as a touch action, you may choose not to inflict damage or gain health. If you do so, your target must make a Fortitude save (DC 10 + 1/2 your Nightjack level + your con mod) or become `Shaken` for 2d4 rounds. You may only inflict a status effect in this way once per target per day. |
| 4 | Cloak of Shadows | Gain the `Cloak` supernatural ability: As a standard action you may fly into a rage, gaining 10ft increased move speed and the `Pounce` ability for a number of rounds equal to 2 plus one fourth of your Nightjack level, rounded down. Recharges on a short rest. |
| 5 |
| 6 | Soul Portal | Gain the `Soul Portal` supernatural ability: As a standard action, make a successful touch attack against a single target creature. If you hit, your body disappears and you live inside the soul of that creature until you decide to leave. You may not attack, use abilities or items, or take any other action during this effect, save to end the effect and leave the host creature. While under the effect of `Soul Portal`, you can access the host creature's senses. The host gains additional hit points equal to twice your Nightjack level, 10ft bonus movement speed, their body is covered with obvious shadowy tendrils to mark your presence. The effect ends when you leave the host creature, or when the host creature dies. The host creature must be medium size and have fewer hit dice than you. If the difference in hit dice is 4 or greater, you take control of the host's movement and physical actions. You may draw weapons, manipulate objects, and perform attacks as the host, but you do not have access to any magic, special, or supernatural abilities posessed by the host and you cannot trigger magical items possessed by the host. Use your own skills when making physical skill checks through the host. This ability recharges after a long rest. |
| 7 | Consumption of Essence | Your `Consume` ability can now affect sentient and non-sentient undead creatures. Whenever you successfully use your `Consume` ability as a touch action, you may make a `Sleight of Hand` check to immediately use `Consume` again as a touch action against a new target. `Sleight of Hand` check is made against the new target's `Spot` check or passive `Perception`. You may not target the same creature more than once in any given round when using your `Consume` ability as a touch action. | 
| 8 | Cloak of Starlight | Your `Cloak` ability now grants spell resistance of 30% and may be used as a swift action. |
| 9 | Curse from Afar | You may use your `Curse` ability by making a ranged touch attack instead of a melee touch attack. The range is 30ft. | 
| 10 |
| 11 | Eldritch Forces | Any non-magical weapon you hold is treated as magical for the purposes of overcoming damage resistance and hitting incorporeal creatures. |
| 12 | Grand Entrance | When under the effect of `Soul Portal`, if the host creature has at least 4 fewer hit dice than you, you may choose to make a `Grand Entrance` when ending the effect. If you do so, `Grand Entrance` instantly kills the host and deals Force damage equal to 1d4 per Nightjack level to all creatures within a 20ft radius. All sentient creatures in view who are not in your party must make a Fortitude save (DC 10 + 1/2 your Nightjack level + your con mod) or `Cower` in fear for 1d4 rounds. Additionally, while under the effect of `Soul Portal` you are invisible to non-magical detection and cannot be affected by spells or magical effects, and you regain a number of hit points equal to your Nightjack level every round. |
| 13 | Curse of Vulnerability | Any creature that makes a successful attack (melee, ranged, touch, or ranged touch) against a target affected by your `Curse` ability gains the effect of your `Consume` ability on that attack. Unsuccessful attacks produce no effect. To recharge `Curse`, you must now successfully `Consume` at least five times instead of twice. |
| 14 | Cloak of Infinity | `Cloak` now grants you immunity to all spells and magical effects and may be used as a free action. |
| 15 | 
| 16 | Consumption of All | When using `Consume` as a touch action, instead of causing the target to be `Shaken`, you may choose to inflict 1d6 points of Ability Damage to the Attribute of your choice. The target must make a fortitude save as normal, but the Ability Damage does not have an artificial duration and instead resolves in the normal way for Ability Damage. |
| 17 | Cloak of the Nightjack | When under the effect of `Soul Portal`, gain the use of the `Cloak of the Nightjack` ability: instead of visibly exiting or making a `Grand Entrance`, you may instead exit by using the `Cloak of the Nightjack`. This costs an activation of your `Cloak` ability, but instead of its normal effects you are invisible to magical and non-magical detection for the duration. Viewers in visual range when you activate this ability get a Reflex save (DC 10 + 1/2 your Nightjack level + your con mod). If they pass, they perceive an effect around the host that clearly indicates your leaving, though they cannot see you directly. |
| 18 | Curse of Creeping Doom | Gain the `Latent Curse` supernatural ability: you may plant a latent version of your `Curse` ability on a touched creature without the target knowing about it. To do so, make a `Sleight of Hand` check against the target's `Spot` check or passive `Perception`. If you succeed, your target gains a `Latent Curse`. If you fail, the target has a 50% chance to know what you were trying to do. After doing this, you may not use this ability or your `Curse` ability again until you have had a short rest. At any time, as a swift action, you may activate any `Latent Curse` you have inflicted on a creature, no matter where the target creature is. Doing so causes the target to immediately suffer the effects of your `Curse` ability as if you had just used it. |
| 19 |
| 20 | Greater Soul Portal | There are no longer size or hit-dice restrictions on what you can target with your `Soul Portal` ability. Additionally, you may now use `Soul Portal` as a swift action and with a ranged touch attack instead of a melee touch attack. Its range is 60ft. |
